package main

import (
	hosts_helpers "app/hosts-helpers"
	"github.com/fatih/color"
	"log"
)

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	//clear stdout
	hosts_helpers.ConsoleClear()

	//check is sudo?
	if !hosts_helpers.IsSudo() {
		color.Red("Please run me with %q", "sudo")
		return
	}

	hosts_helpers.Hosts = make(map[string][]string)
	hosts_helpers.Others = make(map[string][]string)

	//fmt.Println(runtime.GOOS) //так можно узнать какая OS

	lines := hosts_helpers.ReaderFileToLines()
	hosts, _ := hosts_helpers.ParseLines(lines)

	hosts_helpers.ShowLocalHosts(hosts["127.0.0.1"])
	hosts_helpers.AskForAction()

}

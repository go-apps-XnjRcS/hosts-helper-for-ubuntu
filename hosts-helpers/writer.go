package hosts_helpers

import (
	"errors"
	"fmt"
	"github.com/fatih/color"
	"io/ioutil"
	"log"
	"os"
	"strconv"
	"strings"
)

func AskForAction() {
	var variants []string = []string{"1: Show reserved hosts", "2: Add a new"}
	if len(Hosts["127.0.0.1"]) > 0 {
		s := fmt.Sprintf("%d: %s", len(variants)+1, "Delete one")
		variants = append(variants, s)
	}
	s := fmt.Sprintf("%d: %s", len(variants)+1, "Quit")
	variants = append(variants, s)

	answerStr := Asc("So, what are you gonna do?\n", variants)
	answer, _ := strconv.Atoi(answerStr)

	switch answer {
	case 1:
		ConsoleClear()
		ShowLocalHosts(nil)
		AskForAction()

	case 2:
		hostname := Asc("Insert hostname: ", nil)
		AddNewHost(hostname)
		AskForAction()
	case 3:
		ShowLocalHosts(nil)
		numStr := Asc("Insert host number: ", nil)
		num, err := strconv.Atoi(numStr)
		if err != nil {
			color.Red("\n%s\n", err)
			AskForAction()
			break
		}

		_, err = DeleteHost(num)
		if err != nil {
			color.Red("\n%s\n", err)
			AskForAction()
			break
		}

		AskForAction()
	case 4:
		color.Green("Bye-bye, see you later ;-)")
		os.Exit(1)
	default:
		AskForAction()
	}

}

func DeleteHost(n int) (bool, error) {
	hosts := Hosts["127.0.0.1"]

	if len(hosts) < n || n < 0 {
		return false, errors.New("invalid number")
	}

	n--
	host := hosts[n]
	hosts = append(hosts[:n], hosts[n+1:]...)
	Hosts["127.0.0.1"] = hosts

	_, err := WriteToFile()
	if err != nil {
		log.Fatal(err)
	}

	color.Green("\n %q has been deleted successfully.\n", host)
	return true, nil
}

func AddNewHost(host string) {
	host = strings.TrimSpace(host)
	host = strings.ReplaceAll(host, " ", "")

	if IsExists(host) {
		color.Red("Host %q is already exist", host)
		AskForAction()
		return
	}

	q := fmt.Sprintf("Add %q to hosts?\n", host)
	answer := Asc(q, []string{"1 - yes", "2 - no, want to correct"})

	if answer != "1" {
		AskForAction()
		return
	}

	Hosts["127.0.0.1"] = append(Hosts["127.0.0.1"], host)

	_, err := WriteToFile()
	if err != nil {
		log.Fatal(err)
	}

	color.Green("\n%q - added to your hosts\n", host)
}

func WriteToFile() (bool, error) {
	path := "/etc/hosts"
	var data []byte

	for ip, hosts := range Hosts {
		s := fmt.Sprintf("%s %s\n", ip, strings.Join(hosts, " "))
		data = append(data, s...)
	}

	for ip, hosts := range Others {
		s := fmt.Sprintf("%s %s\n", ip, strings.Join(hosts, " "))
		data = append(data, s...)
	}

	err := ioutil.WriteFile(path, data, 0644)
	if err != nil {
		return false, err
	}

	return true, nil
}

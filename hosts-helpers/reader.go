package hosts_helpers

import (
	"bufio"
	"github.com/fatih/color"
	"log"
	"os"
	"strings"
)

var (
	Others map[string][]string //todo need to merge in the future with hosts
	Hosts  map[string][]string
)

func ReaderFileToLines() []string {
	var linesArr []string

	file, err := os.Open("/etc/hosts")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lineStr := scanner.Text()
		linesArr = append(linesArr, lineStr)
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	return linesArr
}

//Parse lines from /etc/hosts and return "hosts" and "others" slices
func ParseLines(lines []string) (map[string][]string, map[string][]string) {
	if len(lines) == 0 {
		return nil, nil
	}

	for _, line := range lines {
		fields := strings.Fields(line)
		if len(fields) < 2 {
			continue
		}

		if fields[0] != "127.0.0.1" {
			Others[fields[0]] = append(Others[fields[0]], fields[1:]...)
			continue
		}

		Hosts[fields[0]] = append(Hosts[fields[0]], fields[1:]...)
	}

	return Hosts, Others
}

func ShowLocalHosts(hosts []string) {
	if hosts == nil {
		hosts = Hosts["127.0.0.1"]
	}

	if len(hosts) == 0 {
		color.Yellow("Hosts are not reserved")
		return
	}

	titleColor := color.New(color.Bold, color.FgCyan)
	_, _ = titleColor.Print("\nYour reserved hosts for 127.0.0.1\n")

	n := 1
	for _, host := range hosts {
		color.Magenta("%d: %s", n, host)
		n++
	}
}

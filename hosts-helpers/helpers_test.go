package hosts_helpers

import "testing"

func TestIsSudo(t *testing.T) {
	if IsSudo() {
		t.Log("Yes that is sudo user")
	} else {
		t.Error("not a sudo user")
	}
}

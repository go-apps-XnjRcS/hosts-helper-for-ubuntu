package hosts_helpers

import (
	"bufio"
	"github.com/fatih/color"
	"log"
	"os"
	"os/exec"
	"strings"
)

func Asc(question string, variants []string) string {
	titleColor := color.New(color.Bold, color.FgCyan)
	_, _ = titleColor.Printf("\n%s", question)

	if len(variants) > 0 {
		for _, item := range variants {
			color.White(item)
		}
	}

	answerStr, _ := bufio.NewReader(os.Stdin).ReadString('\n')
	return strings.TrimSpace(answerStr)
}

func IsExists(host string) bool {

	for _, hosts := range Hosts {
		for _, existedHost := range hosts {
			if existedHost == host {
				return true
			}
		}
	}

	for ip, hosts := range Others {
		if strings.HasPrefix(ip, "#") {
			continue
		}
		for _, existedHost := range hosts {
			if existedHost == host {
				return true
			}
		}
	}

	return false
}

func IsSudo() bool {
	//подробнее https://www.socketloop.com/tutorials/golang-force-your-program-to-run-with-root-permissions
	//а также http://qaru.site/questions/617200/convert-byte-array-to-int-using-go-language/2458440#2458440

	idByte, err := exec.Command("id", "-u").Output()
	//id, _ := strconv.Atoi()
	id := strings.TrimSpace(string(idByte))

	if err != nil {
		log.Fatal(err)
	}

	return id == "0"
}

func ConsoleClear() {
	cmd := exec.Command("clear")
	cmd.Stdout = os.Stdout
	cmd.Run()
}

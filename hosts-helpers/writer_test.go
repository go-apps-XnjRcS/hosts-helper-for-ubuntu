package hosts_helpers

import "testing"

func TestWriteToFile(t *testing.T) {

	res, err := WriteToFile()
	if err != nil {
		t.Error(err)
	} else {
		t.Log(res)
	}
}

## SHT-HOSTS-HELPER

This is a simple **cli** tool that help you to manage **hosts** file.
This is useful for example for developers who need to work with local hosts. And often have to edit /etc/hosts. To simplify this task, I decided to write this simple program. First of all, for personal needs, as well as for educational purposes. 

This program, as well as the code, you can use under a **MIT** license.  

### How to use 
It's a very simple, just look at screencast below

![sht-hosts-helper screencast](https://i.ibb.co/cQD0Hxt/ezgif-4-18b256d56365.gif)


### How to install
- Download [binary file](./bin/sht-hosts-helper).
- `$ sudo chmod +x ./sht-hosts-helper`
- `$ mv sht-hosts-helper /usr/local/bin`
- `$ sudo sht-hosts-helper`

### Create binary from source 
I think this is stupid, maybe it’s worth removing? :-)
- `$ go build -o bin/sht-hosts-helper`



